import * as actionType from '../actions/ActionType';

const initialState = {
  value: {},
  loading: false,
  error: null
};

export const editorReducer = (state = initialState, action) => {
    switch(action.type) {
      case actionType.UPDATE_EDITOR_VALUE:
        return {
          ...state,
          value: action.payload
        };
        default:
          return state;
    }
}
