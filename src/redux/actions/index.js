import * as actionType from './ActionType';

export const updateEditorValue = values => ({
  type: actionType.UPDATE_EDITOR_VALUE,
  payload: {
    data: transform(values), 
    title: values.title
  }
});

const transform = (data) => {
  if(!data.transitions) return {};
  let root = {}
  let visited = {};
  let output = {};
  for (let transition of data.transitions){
    if (root[transition.from] == null){
      root[transition.from] = []
    }
    if(transition.from !== transition.to){
      root[transition.from].push({key: transition.to, event: transition.event })
    }
  }
  output = getChildren(root, Object.keys(root)[0], visited, data.states)
  return output;
 }

  const getChildren = (root, key, visited, states, event={}) => {
    let result = {}
    result.root = key
    result.children = []
    result.display = states[key] || null
    result.event = event
    visited[key] = true
    if(root[key] != null) {
      for (let child of root[key]){
        if(!visited[child.key]){
          result.children = [...result.children, getChildren(root, child.key, visited, states, child.event)]
          // result.event = child.event
        }
      }
    }
    return result;
  }


