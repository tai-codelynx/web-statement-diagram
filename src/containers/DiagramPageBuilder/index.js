import React, { Component } from 'react';
import './Diagram.css';
import { connect } from "react-redux"
import RootComponent from '../../components/UI/Header/Chart/RootChart';

class Diagram extends Component {
  constructor(props){
    super(props)
  }
  
  render() {
    console.log(JSON.stringify(this.props.value.data ))
    return (
        <div className="RootContent">
        <div className="title">
          {this.props.value.title || null}
        </div>
        <RootComponent value={this.props.value.data || {}}/>
        </div>
    );
  }
}

const mapStateToProps = state => ({
  value: state.editorReducer.value
});

export default connect(mapStateToProps)(Diagram);


