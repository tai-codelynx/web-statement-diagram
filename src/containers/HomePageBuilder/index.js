import React, { Component } from 'react';
import './HomePage.css'
import 'antd/dist/antd.css'
import { Layout } from 'antd';
import Header from '../../components/UI/Header'
import Diagram from '../DiagramPageBuilder'
import { Route } from 'react-router-dom';
import AceEditorBuilder from '../AceEditorBuilder';

const { Content } = Layout;

class HomePageBuilder extends Component {
  render() {                  
      return (
        <Layout style={{height: "100vh"}}> 
              <Header logout={this.logout}/>
              <div style={{ display: 'flex', height: '100%'}}>
                <AceEditorBuilder/>
                <Content style={{ padding: 24, background: '#fff', overflow: 'auto'}}>
                    <Route exact path="/" component={Diagram} />
                </Content>  
              </div> 
        </Layout>    
      );
    }   
}

export default HomePageBuilder;