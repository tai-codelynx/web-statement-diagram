import React, { Component } from 'react';
import { connect } from "react-redux"
import CodeEditor from '../../components/AceEditor';
import { bindActionCreators } from 'redux'
import { updateEditorValue } from "../../redux/actions"


class AceEditor extends Component {  
  render() {
    return (
        <div className="LeftMennu" style={{width: 400}}>
            <CodeEditor changeValue={this.props.updateEditorValue}/>
        </div>
    );  
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({ updateEditorValue }, dispatch)

export default connect(null, mapDispatchToProps)(AceEditor);