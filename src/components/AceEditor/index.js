import React from 'react';
import AceEditor from 'react-ace';
import TestParser from '@codelynx/test-parser';
import AwesomeDebouncePromise from 'awesome-debounce-promise';
import 'brace/mode/json';
import 'brace/mode/plain_text';
import 'brace/theme/github';
import './editor.css'

const callSematicParserService = (text) => {
  if (!text) return {};
  try {
    return TestParser.tranform(TestParser.parse(text));
  } catch (error) {
    return null;
  } 
}

const callSematicDebounced = AwesomeDebouncePromise(callSematicParserService, 1000);

class CodeEditor extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      initValue: ""
    }
  }
  
  codeEditorChanging = async (event) => {
    let value = event;
    const sematic = await callSematicDebounced(value);
    if(sematic) {
      this.props.changeValue(sematic);
    }
    this.setState({
      initValue: value
    });
  };

  render() {
    return (
      <AceEditor
        id="code-editor"
        focus={true}
        theme="github"
        showGutter={true}
        highlightActiveLine={true}
        fontSize={14}
        ref="aceEditor"
        setOptions={{
          enableBasicAutocompletion: true,
          enableLiveAutocompletion: true,
          enableSnippets: false,
          showLineNumbers: true,
          tabSize: 2,
        }}
        onChange={(event) => this.codeEditorChanging(event)}
        value={this.state.initValue}
        editorProps={{
          $blockScrolling: Infinity
        }}
      />
    );
  }
}

export default CodeEditor;
