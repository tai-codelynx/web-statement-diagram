import React, { Component } from 'react';
import './App.css';
import { Route,Switch,BrowserRouter } from 'react-router-dom';
import HomePageBuilder from '../../containers/HomePageBuilder';

class App extends Component {
  render() {
    return (
      <div className='App'>
          <BrowserRouter>
            <Switch>            
              <PrivateRoute path="/" component={HomePageBuilder} />
            </Switch>
          </BrowserRouter>
      </div>
    );
  }
}

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => ( <Component {...props} />)} />
)

export default App;
