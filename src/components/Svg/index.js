import React from 'react'

class SvgComponent extends React.Component {

    render() {
        let height = this.props.maxHeight/2
        return(
            <span>
                {this.props.endPoints.map((item, idx) => 
                <svg key={idx} viewBox={`0 0 260 ${this.props.maxHeight}`} xmlns="http://www.w3.org/2000/svg" ref="svg" style={{position: "absolute"}}> 
                    <defs>
                        <marker id="arrowhead" markerWidth="10" markerHeight="7" refX="0" refY="3.5" orient="auto">
                        <polygon points="0 0, 10 3.5, 0 7" fill="#4A4A4A"/>
                        </marker>
                    </defs>
                    <path  id={`${(item.offsetTop + item.height/2) - this.props.offsetTop}_${height}_${idx}`} d={`M2 ${height} L 240 ${(item.offsetTop + item.height/2) - this.props.offsetTop}`} stroke="#4A4A4A" fill="#4A4A4A" strokeWidth="2"  markerEnd="url(#arrowhead)" pointerEvents="all"/>
                    <text fill="#4A4A4A" fontSize="16" fontFamily = "fantasy" fontWeight="600">
                        <textPath href={`#${(item.offsetTop + item.height/2) - this.props.offsetTop}_${height}_${idx}`} startOffset="15%">
                            <tspan dy="-8">{item.label.name ? `#${item.label.name}(${item.label.data.value || item.label.data.target})`: null}</tspan>
                        </textPath> 
                    </text> 
                </svg>
                )}
            </span>
        );
    }
}

export default SvgComponent;



