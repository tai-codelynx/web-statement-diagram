import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Chart from '../Children';
import SvgComponent from '../../../../Svg';
import './RootChart.css'


class RootComponent extends Component {

  componentDidMount(){
    this.renderSVG()
    console.log(JSON.stringify(this.props.value ))
  }

  componentDidUpdate(){
    this.renderSVG()
  }

  renderSVG(){
    ReactDOM.render(
        <SvgComponent
          maxHeight = {this.refs.contentDiv.clientHeight} 
          endPoints = {this._getBoundingClientRect()}
          offsetTop = {this.refs.contentDiv.offsetTop}/>,
      this.refs.svg
    )
  }

  _getBoundingClientRect(){
    let result = []
    this.props.value.children && this.props.value.children.map((item, idx)=> {
      let endpoint = {
        offsetTop: this.refs[`children${idx}`].offsetTop, 
        offsetLeft: this.refs[`children${idx}`].offsetLeft,
        height: this.refs[`children${idx}`].clientHeight,
        width: this.refs[`children${idx}`].clientWidth,
        label: item.event
      }
      result = [...result, endpoint]
    })
    return result
  }

  renderChildren(children) {  
    return(
      children.map((item, idx) => <div key={idx} ref={`children${idx}`}>
        <Chart children={item}/>
      </div>)
    )
  }

  render() {
    const {value} = this.props

    if(typeof(value.root) === "undefined"){
      return (
        <div className="contentDiv" ref="contentDiv">
          <div className="svgDiv" ref="svg"/>
        </div>
      ) 
    }
    return (
     /*  Example
      title Web statement diagram example
      Start->Home :  goto('http://take247-v1.0-master.s3-website-ap-southeast-1.amazonaws.com/')
      Start->Guest: wait(1000)
      Home->Table: click('//*[@id="root"]/div/div/div/div/div[2]/div[1]/div[3]/div[2]/div[2]/button')
      state Home = { html: "<img style='width:200px; height:250px' src='https://s3-ap-southeast-1.amazonaws.com/screenshots-3988/Screenshot_2019-03-20-19-11-10-850_il.co.take247.png'>"}
      state Table = { html: "<table style='width:200px; height: 250px'> <tr> <th>Firstname</th> <th>Lastname</th> <th>Age</th> </tr> <tr> <td>Jill</td> <td>Smith</td> <td>50</td> </tr> <tr> <td>Eve</td> <td>Jackson</td> <td>94</td> </tr> </table>"}
      event click(target)
      = {
        action: 'click',
        target: target
      }
      event goto(url) = {
        action: 'goto',
        value: url
      }
      event wait(time)
      = {
        value: time
      }
      */
      <div className="rootDiv">
        <div className="nodeDiv"> 
            {this.props.value.display ? 
                <div dangerouslySetInnerHTML={{__html: this.props.value.display.html}}/>
                : <div className="divInsideNode"><span>{this.props.value.root}</span></div>}
        </div>
        <div className="contentDiv" ref="contentDiv">
          <div className="svgDiv" ref="svg"/> 
          <div className="childrenDiv">
            {value && this.renderChildren(value.children)}   
          </div>
        </div>
      </div>
    );
  }
}


export default RootComponent;









