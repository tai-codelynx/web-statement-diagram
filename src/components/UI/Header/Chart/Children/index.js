import React, { Component } from 'react';
import RootComponent from '../RootChart';


class Chart extends Component {

  render() {
    let {children} = this.props 
    return (
        <div className="childDiv" style={{ display: 'flex'}}>
          <RootComponent value={children}/>
        </div>
    );
  }
}

export default Chart;