import React from 'react';
import './Header.css'
import {Icon} from 'antd';

const Header = (props) => {
    return (
        <div className="Header">
            <div className="usernameDiv">
                <div className="iconInsideDiv">
                    <Icon type="share-alt" />
                </div>
                <div className="iconInsideDiv" onClick={props.logout}>  
                    <Icon type="setting" />
                </div>
            </div>
        </div>
    );
}

export default Header;